//
//  SchoolDetailsModel.swift
//  20190416-SudhaPolicePatil-NYCSchools
//
//  Created by Sudha P on 17/04/19.
//  Copyright © 2019 Sudha P. All rights reserved.
//

import Foundation

/// Struct created to store School details
struct SchoolDetails {
    var headerValue:String?
    var subValue:String?
    var image:String?
}
