//
//  NYCSchoolServiceResponseModel.swift
//  20190416-SudhaPolicePatil-NYCSchools
//
//  Created by Sudha P on 16/04/19.
//  Copyright © 2019 Sudha P. All rights reserved.
//

import Foundation

/// Class used as a response model for NYC school list.
class NYCSchoolServiceResponseModel: Serializable {
    var building_code:String?
    var bus:String?
    var city:String?
    var code1:String?
    var dbn:String?
    var fax_number:String?
    var latitude:String?
    var location:String?
    var longitude:String?
    var phone_number:String?
    var primary_address_line_1:String?
    var school_email:String?
    var school_name:String?
    var school_sports:String?
    var state_code:String?
    var total_students:String?
    var website:String?
    var zip:String?
    var extracurricular_activities:String?
    var eligibility1:String?
    var overview_paragraph:String?
}


