//
//  Constants.swift
//  APICall
//
//  Created by Sudha P on 16/04/19.
//  Copyright © 2019 Sudha P. All rights reserved.
//

import Foundation

/// Class used for app constant value.
public struct AppConstants{
    public static let getNYCSchoolsDataKey = "getNYCSchoolsData"
    public static let getSATScoreKey = "getSATScore"
    public static let contactDetails = "Contact Details"
    public static let satScore = "SAT Score"
    public static let aboutSchool = "About School"
    public static let activities = "Activities"
    public static let eligiblity = "Eligibility"
    public static let phoneNumber = "Phone Number"
    public static let schoolEmail = "Email ID"
    public static let faxNumber = "Fax Number"
    public static let website = "Website"
    public static let address = "Address"
    
    public static let contactD = "Details"
    public static let SATS = "SAT Score"
    
    public static let dbn = "DBN Number"
    public static let num_of_sat_test_takers = "Number of Test Taker"
    public static let sat_critical_reading_avg_score = "Reading Avarage Score"
    public static let sat_math_avg_score = "Math Avarage Score"
    public static let sat_writing_avg_score = "Writing Avarage Score"
}
