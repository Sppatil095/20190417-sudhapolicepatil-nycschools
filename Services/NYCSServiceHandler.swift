//
//  NYCSServiceHandler.swift
//  20190416-SudhaPolicePatil-NYCSchools
//
//  Created by Sudha P on 16/04/19.
//  Copyright © 2019 Sudha P. All rights reserved.
//

import Foundation

/// This class used to make service call by fetching URL from Utility getUrl Method.
class NYCSServiceHandler {
    
    /// This method used to get the NYC High School List by making respected Service Cal.
    ///
    /// - Parameters:
    ///   - onSuccess: It's callBack on service success method. It contains the response model.
    ///   - onFailure: It's  callBack for service failure if there is any failure on service call.
    class func getNYCSchoolsData(onSuccess: @escaping ([NYCSchoolServiceResponseModel]) -> Void, onFailure: @escaping (_ error: String?) -> Void){
        let getSchoolListURL = Utilities.getURLWith(key: AppConstants.getNYCSchoolsDataKey) ?? ""
        let schoolListService = NYCSBaseServices(serviceType: .GET, serviceURL: getSchoolListURL)
        schoolListService.start(onSuccess: { (responseData) in
            if let schoolListModel = try? JSONDecoder().decode([NYCSchoolServiceResponseModel].self, from: responseData) {
                onSuccess(schoolListModel)
            }else{
                onFailure(nil)
            }
        }) { (error) in
            onFailure(error.localizedDescription)
        }
    }

    
    /// This method used to get the SAT Score of the NYC High School by making respected Service Cal.
    ///
    /// - Parameters:
    ///   - onSuccess: It's callBack on service success method. It contains the response model,
    ///   - onFailure: It's  callBack for service failure if there is any failure on service call.
    class func getSATScoreData(onSuccess: @escaping ([NYCSchoolSATScoreResponseModel]) -> Void, onFailure: @escaping (_ error: String?) -> Void){
        let satScoreURL = Utilities.getURLWith(key: AppConstants.getSATScoreKey) ?? ""
        let satScoreService = NYCSBaseServices(serviceType: .GET, serviceURL: satScoreURL)
        satScoreService.start(onSuccess: { (responseData) in
            if let satScore = try? JSONDecoder().decode([NYCSchoolSATScoreResponseModel].self, from: responseData) {
                onSuccess(satScore)
            }else{
                onFailure(nil)
            }
        }) { (error) in
            onFailure(error.localizedDescription)
        }
    }
    
}
