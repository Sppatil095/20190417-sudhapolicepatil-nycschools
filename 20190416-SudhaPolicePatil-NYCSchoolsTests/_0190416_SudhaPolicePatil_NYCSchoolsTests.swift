//
//  _0190416_SudhaPolicePatil_NYCSchoolsTests.swift
//  20190416-SudhaPolicePatil-NYCSchoolsTests
//
//  Created by Sudha P on 16/04/19.
//  Copyright © 2019 Sudha P. All rights reserved.
//

import XCTest
@testable import _0190416_SudhaPolicePatil_NYCSchools

class _0190416_SudhaPolicePatil_NYCSchoolsTests: XCTestCase {
    var nycSchoolListViewController : NYCSchoolListViewController? = nil
    var nycSchoolServiceResponseModel:[NYCSchoolServiceResponseModel]?
    let tableView = UITableView()
    
    var schoolListCell :NYCSCustomTableViewCell? = nil
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        nycSchoolListViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NYCSchoolListViewController") as? NYCSchoolListViewController
        _ = nycSchoolListViewController?.view
        _ = nycSchoolListViewController?.schoolTableView
        
        
        schoolListCell = nycSchoolListViewController?.schoolTableView.dequeueReusableCell(withIdentifier: String(describing: NYCSCustomTableViewCell.self)) as? NYCSCustomTableViewCell
        
        let testBundle = Bundle(for: type(of: self))
        let path = testBundle.path(forResource: "NYCSchoolDetailsResponse", ofType: "json")
        let responseData = TCUtility.jsonForFileAtPath(path!)
        if let schoolDetails = try? JSONDecoder().decode([NYCSchoolServiceResponseModel].self, from: responseData!) {
            nycSchoolServiceResponseModel = schoolDetails
            if let nycSchoolServiceResponseModel = nycSchoolServiceResponseModel {
                nycSchoolListViewController?.nycSchoolDetailsModel =  nycSchoolServiceResponseModel
            }
        }
    }
    
    func testIsControllerNotNil(){
        XCTAssertNotNil(nycSchoolListViewController, "Is controller not nil")
    }
    
    func textAlertMethod(){
        XCTAssertNotNil(nycSchoolListViewController?.showErrorAlert(), "Created Alert") 
    }
    
    func testNumberOfSection(){
        XCTAssertEqual(nycSchoolListViewController?.numberOfSections(in: nycSchoolListViewController?.schoolTableView ?? tableView), 1)
    }
    
    func testNumberOfRowsInSection(){
        XCTAssertNotEqual(nycSchoolListViewController?.tableView(nycSchoolListViewController?.schoolTableView ?? tableView, numberOfRowsInSection: 1), 5)
    }
    
    func testCellForRowAt(){
        let indexPath = IndexPath(row: 1, section: 1)
        let customCell = nycSchoolListViewController?.tableView(nycSchoolListViewController?.schoolTableView ?? tableView, cellForRowAt: indexPath)
        XCTAssertNotNil(customCell, "Set Cell Height")
    }
    
    func testIscustomCellCreated(){
        XCTAssertNotNil(schoolListCell, "Is Custom Cell Created")
    }
    
    func testCustomCellSelectedMethod(){
        XCTAssertNotNil(schoolListCell?.setSelected(true, animated: true), "Table does not create reusable cells")
    }

    func testDidSelectRowAt(){
        let indexPath = IndexPath(row: 1, section: 1)
        XCTAssertNotNil(nycSchoolListViewController?.tableView(nycSchoolListViewController?.schoolTableView ?? tableView, didSelectRowAt: indexPath), "On Select Tablewview cell")
    }
    
    func testDidReceiveResponseData(){
        let model = [NYCSchoolServiceResponseModel]()
        XCTAssertNotNil(nycSchoolListViewController?.didReceiveSchoolData(responseModel:self.nycSchoolServiceResponseModel ?? model), "Passing response model from Mock json")
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        self.nycSchoolServiceResponseModel = nil
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testSchoolResponseModelDateCount(){
        XCTAssertNotEqual(nycSchoolListViewController?.nycSchoolDetailsModel.count, 0)
    }
    
}
