//
//  TCUtility.swift
//  20190416-SudhaPolicePatil-NYCSchoolsTests
//
//  Created by Sudha P on 17/04/19.
//  Copyright © 2019 Sudha P. All rights reserved.
//

import UIKit

class TCUtility: NSObject {
    open class func jsonForFileAtPath(_ filePath:String) -> Data?{
        do {
            let text = try NSString(contentsOfFile: filePath,encoding: String.Encoding.ascii.rawValue)
            return text.data(using: String.Encoding.utf8.rawValue, allowLossyConversion: false)
        } catch {
            print("Catch block executed")
        }
        return nil
    }
}
