//
//  NYCSchoolDetailsViewControllerTestCases.swift
//  20190416-SudhaPolicePatil-NYCSchoolsTests
//
//  Created by Sudha P on 17/04/19.
//  Copyright © 2019 Sudha P. All rights reserved.
//

import XCTest
@testable import _0190416_SudhaPolicePatil_NYCSchools

class NYCSchoolDetailsViewControllerTestCases: XCTestCase {

    var nycSchoolDetailsViewController : NYCSchoolDetailsViewController? = nil
    var nycSchoolServiceResponseModel:[NYCSchoolServiceResponseModel]?
    let tableView = UITableView()
    var activityCell:SchoolDetailsTableViewCell? = nil
    var descriptionCell:SchoolDescriptionTableViewCell? = nil
    let model = NYCSchoolServiceResponseModel()
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        nycSchoolDetailsViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NYCSchoolDetailsViewController") as? NYCSchoolDetailsViewController
        _ = nycSchoolDetailsViewController?.view
        _ = nycSchoolDetailsViewController?.schoolTableView
        
        let testBundle = Bundle(for: type(of: self))
        let path = testBundle.path(forResource: "NYCSchoolDetailsResponse", ofType: "json")
        let responseData = TCUtility.jsonForFileAtPath(path!)
        if let schoolDetails = try? JSONDecoder().decode([NYCSchoolServiceResponseModel].self, from: responseData!) {
            nycSchoolServiceResponseModel = schoolDetails
            if let nycSchoolServiceResponseModel = nycSchoolServiceResponseModel {
                nycSchoolDetailsViewController?.nycSchoolDetailsModel =  nycSchoolServiceResponseModel[0]
            }
        }
        
        activityCell = nycSchoolDetailsViewController?.schoolTableView.dequeueReusableCell(withIdentifier: String(describing: SchoolDetailsTableViewCell.self)) as? SchoolDetailsTableViewCell
        
        descriptionCell = nycSchoolDetailsViewController?.schoolTableView.dequeueReusableCell(withIdentifier: String(describing: SchoolDescriptionTableViewCell.self)) as? SchoolDescriptionTableViewCell
    }
    
    func testIsControllerNotNil(){
        XCTAssertNotNil(nycSchoolDetailsViewController, "Is controller not nil")
    }
    
    func testIsViewLoded(){
        XCTAssertNotNil(nycSchoolDetailsViewController?.viewDidLoad(), "Is view loaded")
    }
    
    func testNumberOfSection(){
        XCTAssertEqual(nycSchoolDetailsViewController?.numberOfSections(in: nycSchoolDetailsViewController?.schoolTableView ?? tableView), 1)
    }
    
    func testNumberOfRowsInSection(){
    XCTAssertEqual(nycSchoolDetailsViewController?.tableView(nycSchoolDetailsViewController?.schoolTableView ?? tableView, numberOfRowsInSection: 1), 5)
    }
    
    func testCellForRowAt(){
        let indexPath = IndexPath(row: 1, section: 1)
        XCTAssertNotNil(nycSchoolDetailsViewController?.tableView(nycSchoolDetailsViewController?.schoolTableView ?? tableView, cellForRowAt: indexPath), "Set Cell Height")
    }
    
    func testCellForRowAtDifferentIndex(){
        let indexPath = IndexPath(row: 2, section: 1)
        XCTAssertNotNil(nycSchoolDetailsViewController?.tableView(nycSchoolDetailsViewController?.schoolTableView ?? tableView, cellForRowAt: indexPath), "Set Cell Height")
    }
    
//    func testHeightForRowAt(){
//       let indexPath = IndexPath(row: 1, section: 1)
//    XCTAssertNotNil(nycSchoolDetailsViewController?.tableView(nycSchoolDetailsViewController?.schoolTableView ?? tableView, heightForRowAt: indexPath), "Set Cell Height")
//    }
    
    func testDidSelectRowAt(){
        let indexPath = IndexPath(row: 1, section: 1)
        XCTAssertNotNil(nycSchoolDetailsViewController?.tableView(nycSchoolDetailsViewController?.schoolTableView ?? tableView, didSelectRowAt: indexPath), "On Select Tablewview cell")
    }
    
    func testCustomCellCreated(){
        XCTAssertNotNil(activityCell, "Is Customcell Created")
    }
    
    func testNYCCustomTableViewCellSetSelected(){
        XCTAssertNotNil(activityCell?.setSelected(true, animated: true), "Table does not create reusable cells")
    }
    
    func testdescriptionCellCreated(){
        XCTAssertNotNil(descriptionCell, "Is Customcell Created")
    }
    
    func testNYCdescriptionCellSetSelected(){
        XCTAssertNotNil(descriptionCell?.setSelected(true, animated: true), "Table does not create reusable cells")
    }
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        self.nycSchoolServiceResponseModel = nil
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

    
}
