//
//  NYCSchoolContactDetailsViewControllerTestCases.swift
//  20190416-SudhaPolicePatil-NYCSchoolsTests
//
//  Created by Sudha P on 17/04/19.
//  Copyright © 2019 Sudha P. All rights reserved.
//

import XCTest
@testable import _0190416_SudhaPolicePatil_NYCSchools

class NYCSchoolContactDetailsViewControllerTestCases: XCTestCase {

    var nycSchoolContactDetailsViewController : NYCSchoolContactDetailsViewController? = nil
    var nycSchoolServiceResponseModel:[NYCSchoolServiceResponseModel]?
    var nycSchoolSATScoreResponseModel:[NYCSchoolSATScoreResponseModel]?
    let tableView = UITableView()
    
    var detailsCell:SchoolInfoTableViewCell? = nil
    let model = NYCSchoolServiceResponseModel()
    let satmModel = NYCSchoolSATScoreResponseModel()
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        nycSchoolContactDetailsViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NYCSchoolContactDetailsViewController") as? NYCSchoolContactDetailsViewController
        _ = nycSchoolContactDetailsViewController?.view
        _ = nycSchoolContactDetailsViewController?.contactDetailstableView
        
        let testBundle = Bundle(for: type(of: self))
        let path = testBundle.path(forResource: "NYCSchoolDetailsResponse", ofType: "json")
        let responseData = TCUtility.jsonForFileAtPath(path!)
        if let schoolDetails = try? JSONDecoder().decode([NYCSchoolServiceResponseModel].self, from: responseData!) {
            nycSchoolServiceResponseModel = schoolDetails
            if let nycSchoolServiceResponseModel = nycSchoolServiceResponseModel {
                nycSchoolContactDetailsViewController?.nycSchoolDetailsModel =  nycSchoolServiceResponseModel[0]
            }
        }

        let pathScore = testBundle.path(forResource: "SATScoreResponse", ofType: "json")
        let responseOfSATScore = TCUtility.jsonForFileAtPath(pathScore!)
        if let satDetails = try? JSONDecoder().decode([NYCSchoolSATScoreResponseModel].self, from: responseOfSATScore!) {
            nycSchoolSATScoreResponseModel = satDetails
            if let nycSchoolSATScoreResponseModel = nycSchoolSATScoreResponseModel {
                nycSchoolContactDetailsViewController?.nycSchoolSATScoreResponseModel =  nycSchoolSATScoreResponseModel
            }
        }
        
        
        detailsCell = nycSchoolContactDetailsViewController?.contactDetailstableView.dequeueReusableCell(withIdentifier: String(describing: SchoolInfoTableViewCell.self)) as? SchoolInfoTableViewCell
    }
    
    func testIsControllerNotNil(){
        XCTAssertNotNil(nycSchoolContactDetailsViewController, "Is controller not nil")
    }
    
    func testIsViewAppear(){
        XCTAssertNotNil(nycSchoolContactDetailsViewController?.viewWillAppear(true), "Is view Appear called")
    }
    
    func testIsViewLoded(){
        XCTAssertNotNil(nycSchoolContactDetailsViewController?.viewDidLoad(), "Is view loaded")
    }
    
    func testGetSATScore(){
        XCTAssertNotNil(nycSchoolContactDetailsViewController?.getSATScoreDetails(), "Is Get SatScore function calling")
    }
    
    func testIsGetIndexoftheElement(){
        XCTAssertNotNil(nycSchoolContactDetailsViewController?.getIndexOfTheElement(), "Is Get index of the element function calling")
    }
    
    func testNumberOfSection(){
        XCTAssertEqual(nycSchoolContactDetailsViewController?.numberOfSections(in: nycSchoolContactDetailsViewController?.contactDetailstableView ?? tableView), 1)
    }
    
    func testNumberOfRowsInSection(){
        XCTAssertEqual(nycSchoolContactDetailsViewController?.tableView(nycSchoolContactDetailsViewController?.contactDetailstableView ?? tableView, numberOfRowsInSection: 1), 5)
    }
    
    func testCellForRowAt(){
        let indexPath = IndexPath(row: 1, section: 1)
        XCTAssertNotNil(nycSchoolContactDetailsViewController?.tableView(nycSchoolContactDetailsViewController?.contactDetailstableView ?? tableView, cellForRowAt: indexPath), "Set Cell Height")
    }
    
//    func testHeightForRowAt(){
//        let indexPath = IndexPath(row: 1, section: 1)
//        XCTAssertNotNil(nycSchoolContactDetailsViewController?.tableView(nycSchoolContactDetailsViewController?.contactDetailstableView ?? tableView, heightForRowAt: indexPath), "Set Cell Height")
//    }
    
    func testDescriptionTableViewCellSetSelected(){
        XCTAssertNotNil(detailsCell?.setSelected(true, animated: true), "Table does not create reusable cells")
    }
    
    func testServicHandler(){
        XCTAssertEqual(NYCServiceErrorHadler.sharedInstance.showServiceError, true)
    }
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        self.nycSchoolServiceResponseModel = nil
        self.nycSchoolSATScoreResponseModel = nil
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
