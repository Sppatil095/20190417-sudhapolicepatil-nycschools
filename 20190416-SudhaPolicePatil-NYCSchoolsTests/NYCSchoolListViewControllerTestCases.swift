//
//  NYCSchoolListViewControllerTestCases.swift
//  20190416-SudhaPolicePatil-NYCSchoolsTests
//
//  Created by Sudha P on 17/04/19.
//  Copyright © 2019 Sudha P. All rights reserved.
//

import XCTest
@testable import _0190416-SudhaPolicePatil-NYCSchools

class NYCSchoolListViewControllerTestCases: XCTestCase {

    var nycSchoolListViewController : NYCSchoolListViewController? = nil
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        nycSchoolListViewController = UIStoryboard(name: "Store", bundle: nil).instantiateViewController(withIdentifier: "NYCSchoolListViewController") as? NYCSchoolListViewController
        _ = nycSchoolListViewController?.view
        _ = nycSchoolListViewController?.tableView
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
