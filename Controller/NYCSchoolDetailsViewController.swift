//
//  NYCSchoolDetailsViewController.swift
//  20190416-SudhaPolicePatil-NYCSchools
//
//  Created by Sudha P on 16/04/19.
//  Copyright © 2019 Sudha P. All rights reserved.
//

import UIKit
/**
 The purpose of the `NYCSchoolDetailsViewController` view controller is used to show the complete details of the selected school.
 
 - This will appear when user select any school from the list.
 
 There's a matching scene in the *Main.storyboard* file, Go to Interface Builder for details.
 
 The `NYCSchoolListViewController` class is a subclass of the `UIViewController`.
 */

/// Enum created for cell type
///
/// - ContactDetails: Contains Contact Details
/// - SatScore: Contains SAT Score Details
/// - Activities: Contains Activity Details
/// - Elgibility: Contains Eligibility Details
/// - AboutSchool: Contains School Details
enum SelectedTypeEnum: Int {
    case ContactDetails = 0
    case SatScore
    case Activities
    case Elgibility
    case AboutSchool
}

class NYCSchoolDetailsViewController: UIViewController {

    /// Outlect created to show the school name.
    @IBOutlet weak var schoolNameLabel: UILabel!
    /// Outlect created to show the address.
    @IBOutlet weak var addressLabel: UILabel!
    /// Outlet created for table view. Which is used to show the school details based on the service response.
    @IBOutlet weak var schoolTableView: UITableView!
    /// Property used to get selected school response model.
    var nycSchoolDetailsModel: NYCSchoolServiceResponseModel?
    
    /// Model used to store array of SchoolDetails.
    var schoolModel:[SchoolDetails] = [SchoolDetails]()
    /// Called after the controller's view is loaded into memory.
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if let nycSchoolDetailsModel = nycSchoolDetailsModel {
            self.schoolNameLabel.text = nycSchoolDetailsModel.school_name
            self.addressLabel.text = nycSchoolDetailsModel.location?.components(separatedBy: "(").first
        }
        
        self.initialSetUp()
    }

    /// Sent to the view controller when the app receives a memory warning.
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    /// Method used to make initial setup for the controller. It contains loading nib file to the tableview and tableview setup.
    func initialSetUp(){
        let nibNameForSchoolCell = UINib(nibName: String(describing: SchoolDetailsTableViewCell.self) , bundle: nil)
        schoolTableView.register(nibNameForSchoolCell, forCellReuseIdentifier: String(describing: SchoolDetailsTableViewCell.self))
        
        let nibNameForSchoolDCell = UINib(nibName: String(describing: SchoolDescriptionTableViewCell.self) , bundle: nil)
        schoolTableView.register(nibNameForSchoolDCell, forCellReuseIdentifier: String(describing: SchoolDescriptionTableViewCell.self))
        schoolTableView.dataSource = self
        schoolTableView.delegate = self
        schoolTableView.estimatedRowHeight = 70
        createDetailsModel()
    }
    
    func createDetailsModel(){
        self.schoolModel.append(SchoolDetails(headerValue:AppConstants.contactDetails, subValue:"", image: ""))
        self.schoolModel.append(SchoolDetails(headerValue:AppConstants.satScore, subValue:"", image: ""))
        self.schoolModel.append(SchoolDetails(headerValue:AppConstants.activities, subValue:nycSchoolDetailsModel?.extracurricular_activities ?? "", image: ""))
        self.schoolModel.append(SchoolDetails(headerValue:AppConstants.eligiblity, subValue:nycSchoolDetailsModel?.eligibility1 ?? "", image: ""))
        self.schoolModel.append(SchoolDetails(headerValue:AppConstants.aboutSchool, subValue:nycSchoolDetailsModel?.overview_paragraph ?? "", image: ""))
    }

}

// MARK: - UITableViewDelegate and UITableViewDataSource
extension NYCSchoolDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    
    /// Asks the data source to return the number of sections in the table view.
    ///
    /// - Parameter tableView: An object representing the table view requesting this information.
    /// - Returns: The number of sections in tableView. The default value is 1.
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    /// Tells the data source to return the number of rows in a given section of a table view.
    ///
    /// - Parameters:
    ///   - tableView: The table-view object requesting this information.
    ///   - section: An index number identifying a section in tableView.
    /// - Returns: The number of rows in section.
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.schoolModel.count
    }
    
    /// Asks the data source for a cell to insert in a particular location of the table view.
    ///
    /// - Parameters:
    ///   - tableView: A table-view object requesting the cell.
    ///   - indexPath: An index path locating a row in tableView.
    /// - Returns: An object inheriting from UITableViewCell that the table view can use for the specified row. An assertion is raised if you return nil.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == SelectedTypeEnum.ContactDetails.rawValue || indexPath.row == SelectedTypeEnum.SatScore.rawValue {
            let activityCell = tableView.dequeueReusableCell(withIdentifier: String(describing: SchoolDetailsTableViewCell.self)) as! SchoolDetailsTableViewCell
            if self.schoolModel.count > 0 {
                activityCell.activitylabel.text = self.schoolModel[indexPath.row].headerValue
            }
            activityCell.isUserInteractionEnabled = true
            activityCell.selectionStyle = UITableViewCellSelectionStyle.none
            return activityCell
        }else {
           let descriptionCell = tableView.dequeueReusableCell(withIdentifier: String(describing: SchoolDescriptionTableViewCell.self)) as! SchoolDescriptionTableViewCell
            if self.schoolModel.count > 0 {
                descriptionCell.activityLabel.text = self.schoolModel[indexPath.row].headerValue
                descriptionCell.descriptionLabel.text = self.schoolModel[indexPath.row].subValue
            }
            descriptionCell.selectionStyle = UITableViewCellSelectionStyle.none
            descriptionCell.isUserInteractionEnabled = false
            return descriptionCell
        }
    }
    
    /// Tells the delegate that the specified row is now selected.
    ///
    /// - Parameters:
    ///   - tableView: A table-view object informing the delegate about the new row selection.
    ///   - indexPath: An index path locating the new selected row in tableView.
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let schoolContactDetailsViewController = mainStoryboard.instantiateViewController(withIdentifier: String(describing: NYCSchoolContactDetailsViewController.self)) as! NYCSchoolContactDetailsViewController
        schoolContactDetailsViewController.nycSchoolDetailsModel = self.nycSchoolDetailsModel
        schoolContactDetailsViewController.selectedCellType = indexPath.row == 0 ? SelectedTypeEnum.ContactDetails : SelectedTypeEnum.SatScore
        navigationController?.pushViewController(schoolContactDetailsViewController, animated: true)
    }
}
