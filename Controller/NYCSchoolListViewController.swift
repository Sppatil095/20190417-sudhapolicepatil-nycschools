//
//  NYCSchoolListViewController.swift
//  20190416-SudhaPolicePatil-NYCSchools
//
//  Created by Sudha P on 16/04/19.
//  Copyright © 2019 Sudha P. All rights reserved.
//

import UIKit

/**
 The purpose of the `NYCSchoolListViewController` view controller is used to show the list of NYC High School.
 
 - This will appear when user opens the app.
 
 There's a matching scene in the *Main.storyboard* file, Go to Interface Builder for details.
 
 The `NYCSchoolListViewController` class is a subclass of the `UIViewController`.
 */
class NYCSchoolListViewController: UIViewController {

    /// Outlet created for table view. Which is used to show the school list based on the service response.
    @IBOutlet weak var schoolTableView: UITableView!
    
    /// Outlet created for activitiIndicator. Which is used to show the animation on making the service call.
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    /// Outlet created for UILabel.Which is used to show the "featching data" while making service call.
    @IBOutlet weak var defaultLabel: UILabel!
    
    /// Property created as array of NYCSchoolServiceResponseModel. Which contains the list of School list.
    var nycSchoolDetailsModel: [NYCSchoolServiceResponseModel] = []
    
    /// Called after the controller's view is loaded into memory.
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.initialSetUp()
    }

    /// Sent to the view controller when the app receives a memory warning.
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    /// Method used to make initial setup for the controller. It contains loading nib file to the tableview and tableview setup.
    func initialSetUp(){
        let nibNameForSchoolCell = UINib(nibName: String(describing: NYCSCustomTableViewCell.self) , bundle: nil)
        schoolTableView.register(nibNameForSchoolCell, forCellReuseIdentifier: String(describing: NYCSCustomTableViewCell.self))
        schoolTableView.dataSource = self
        schoolTableView.delegate = self
        
        schoolTableView.estimatedRowHeight = 128.0
        self.activityIndicator.startAnimating()
        self.activityIndicator.isHidden = false
        self.schoolTableView.isHidden = true
        self.defaultLabel.isHidden = false
        
        DispatchQueue.main.async {
            self.getNYCSchoolDataList()
        }
    }
    
    /// Method used to make service call to get the school list.
    func getNYCSchoolDataList(){
        NYCSServiceHandler.getNYCSchoolsData(onSuccess: { (responseModel) in
            self.didReceiveSchoolData(responseModel:responseModel)
        }, onFailure: { (error) in
            print(error ?? "")
            self.activityIndicator.isHidden = true
            self.defaultLabel.isHidden = true
            self.showErrorAlert()
        })
    }
    
    /// Method used to show the alert infor to the user if there is any failure in service cal.
    func showErrorAlert() {
        let alert = UIAlertController(title: "TimeOut", message: "Something went wrong. Please check your internet connection.",         preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Ok",
                                      style: UIAlertActionStyle.default,
                                      handler: {(_: UIAlertAction!) in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    /// Method called when get NYC School service response success.
    ///
    /// - Parameter responseModel: Contains array of NYCSchoolServiceResponseModel response model.
    func didReceiveSchoolData(responseModel:[NYCSchoolServiceResponseModel]){
        self.nycSchoolDetailsModel = responseModel
        self.activityIndicator.stopAnimating()
        self.activityIndicator.isHidden = true
        self.defaultLabel.isHidden = true
        self.schoolTableView.isHidden = false
        DispatchQueue.main.async {
            self.schoolTableView.reloadData()
        }
    }

}


// MARK: - UITableViewDelegate and UITableViewDataSource
extension NYCSchoolListViewController: UITableViewDelegate, UITableViewDataSource {
    
    /// Asks the data source to return the number of sections in the table view.
    ///
    /// - Parameter tableView: An object representing the table view requesting this information.
    /// - Returns: The number of sections in tableView. The default value is 1.
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    /// Tells the data source to return the number of rows in a given section of a table view.
    ///
    /// - Parameters:
    ///   - tableView: The table-view object requesting this information.
    ///   - section: An index number identifying a section in tableView.
    /// - Returns: The number of rows in section.
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.nycSchoolDetailsModel.count
    }
    
    /// Asks the data source for a cell to insert in a particular location of the table view.
    ///
    /// - Parameters:
    ///   - tableView: A table-view object requesting the cell.
    ///   - indexPath: An index path locating a row in tableView.
    /// - Returns: An object inheriting from UITableViewCell that the table view can use for the specified row. An assertion is raised if you return nil.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let schoolListCell = tableView.dequeueReusableCell(withIdentifier: String(describing: NYCSCustomTableViewCell.self)) as! NYCSCustomTableViewCell
        if self.nycSchoolDetailsModel.count > 0 {
            schoolListCell.schoolNameLabel.text = self.nycSchoolDetailsModel[indexPath.row].school_name
            schoolListCell.schoolLocationLabel.text = self.nycSchoolDetailsModel[indexPath.row].location?.components(separatedBy: "(").first
        }
        schoolListCell.selectionStyle = UITableViewCellSelectionStyle.none
        return schoolListCell
    }

    
    /// Tells the delegate that the specified row is now selected.
    ///
    /// - Parameters:
    ///   - tableView: A table-view object informing the delegate about the new row selection.
    ///   - indexPath: An index path locating the new selected row in tableView.
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let schoolDetailsViewController = mainStoryboard.instantiateViewController(withIdentifier: String(describing: NYCSchoolDetailsViewController.self)) as! NYCSchoolDetailsViewController
        if self.nycSchoolDetailsModel.count > 0 {
            schoolDetailsViewController.nycSchoolDetailsModel = self.nycSchoolDetailsModel[indexPath.row]
        }
        navigationController?.pushViewController(schoolDetailsViewController, animated: true)
    }
}
