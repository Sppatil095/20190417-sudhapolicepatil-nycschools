//
//  NYCSCustomTableViewCell.swift
//  20190416-SudhaPolicePatil-NYCSchools
//
//  Created by Sudha P on 16/04/19.
//  Copyright © 2019 Sudha P. All rights reserved.
//

import UIKit

/// Cell used to show the school name and location.
class NYCSCustomTableViewCell: UITableViewCell {
    
    /// Outlet created to show the school name on the cell.
    @IBOutlet weak var schoolNameLabel: UILabel!
    /// Outlet created to show the location of the school.
    @IBOutlet weak var schoolLocationLabel: UILabel!
    
    /// Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file.
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    /// Sets the selected state of the cell, optionally animating the transition between states.
    ///
    /// - Parameters:
    ///   - selected: true to set the cell as selected, false to set it as unselected. The default is false.
    ///   - animated: true to animate the transition between selected states, false to make the transition immediate.
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
